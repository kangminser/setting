import { GraphQLServer } from "graphql-yoga";
import resolvers from "./graphql/resolvers";
import express from "express";
import path from "path";
import session from "express-session";
// import ms from "ms";
// var redis = require("redis");
// var redisStore = require("connect-redis")(session);
// var client = redis.createClient();

const context = req => ({
  req: req.request
});

const server = new GraphQLServer({
  typeDefs: "graphql/schema.graphql",
  resolvers,
  context
});

const options = {
  port: 5000,
  endpoint: "/api",
  playground: "/play",
  cors: {
    credentials: true,
    origin: ["http://localhost:3000"] // your frontend url.
  }
};

// session middleware
server.express.use(
  session({
    name: "kmsadminServer",
    secret: `kmsadmin`,
    resave: false,
    saveUninitialized: true
    // store: new redisStore({
    //   host: "localhost",
    //   port: 6379,
    //   client: client,
    //   ttl: 260
    // })
    // cookie: {
    //     // 운영이 https 가 아니으로 삭제
    //   //secure: process.env.NODE_ENV === 'production',
    // //   httpOnly:true,
    // //   maxAge: ms('1d'),
    // },
  })
);

// server.use(express.static(path.join(__dirname, "build")));

// server.get("*", (req, res) => {
//   res.sendFile(path.join(__dirname, "build", "index.html"));
// });

server.start(options, () => console.log("Graphql Server Running"));
