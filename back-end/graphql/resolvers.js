import {
  people,
  getTest
} from "./db";

const resolvers = {
  Query: {
    people: () => people,
    getTest: () => getTest()
  }
};

export default resolvers;
