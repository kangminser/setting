import React from "react";
import { Router } from "react-router-dom";
import { RouteConfig } from "./RouteConfig";
import { ApolloProvider } from "react-apollo-hooks";
import { createBrowserHistory } from "history";
import { createUploadLink } from "apollo-upload-client";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";

const link = createUploadLink({ uri: "/api" });

const cache = new InMemoryCache({
  addTypename: false
});

const client = new ApolloClient({
  link,
  cache: cache
});

const history = createBrowserHistory();

const App = () => (
  <ApolloProvider client={client}>
    <Router history={history}>
      <RouteConfig />
    </Router>
  </ApolloProvider>
);

export default App;
