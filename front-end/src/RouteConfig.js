import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./routes/HomeRouter";

const routes = [
  {
    exact: true,
    path: "/",
    component: Home
  }
];
const RouteWithSubRoutes = route => {
  return (
    <Route
      exact={route.exact}
      path={route.path}
      render={props => <route.component {...props} routes={route.routes} />}
    />
  );
};

const RouteConfig = () => {
  return (
    <Switch>
      {routes.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route} />
      ))}
    </Switch>
  );
};

//export default RouteConfig
export {
  routes,
  RouteConfig,
  RouteWithSubRoutes
  // Notfound
};
