import React from "react";
import { useQuery } from "react-apollo-hooks";
import gql from "graphql-tag";
import { Link } from "react-router-dom";
import classNames from "classnames/bind";
import style from "./Home.module.scss";
const cx = classNames.bind(style);

const QUERY = gql`
  query getPeople {
    people {
      id
      name
      age
      gender
    }
  }
`;

function Home() {
  const { data, loading, error } = useQuery(QUERY, {
    fetchPolicy: "network-only"
  });
  if (loading) {
    return <div />;
  }
  if (error) {
    return <div />;
  }
  return (
    <div>
      {data.people &&
        data.people.map(({ id, name, age, gender }, i) => {
          return (
            <div key={i} className={cx("test")}>
              {id} / {name} / {age} / {gender}{" "}
            </div>
          );
        })}
      <Link to={"/test"}>test</Link>
    </div>
  );
}

export default Home;
